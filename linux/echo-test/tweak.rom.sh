#!/bin/bash
C4='\033[00;34m'
C5='\033[00;32m'
BD='\033[00;1m'
RC='\033[0m'

date
host="10.0.2.15"
host="localhost"
printf "Setting up ${C4}ssh${RC} context on : ${C4}${host}${RC} host\n"

forwardPort=1440
sshCommand="ssh"
sshCommand="ssh -p ${forwardPort}"

# mkdir .ssh;vi /etc/default/dropbear; /etc/init.d/dropbear stop; /etc/init.d/dropbear start

cat ~/.ssh/id_rsa.pub | ${sshCommand} root@${host} 'cat >> .ssh/authorized_keys'
${sshCommand} root@${host} 'ls -al -h'

[ "$?" == 0 ] && 
{ 
	printf "\n${C4}${host}${RC} ssh setup: ${C5}success${RC}\nopenAMP setup can be applied\n"; 
	OpenAMPDir="/opt/shared/bin/openAMP"
	${sshCommand} root@${host} "mkdir -p ${OpenAMPDir}"
	scp -P ${forwardPort} ZynQopenAMP.sh root@${host}:${OpenAMPDir}
	printf "Initialising ${C4}${host}${RC} openAMP framework\n"; 
	${sshCommand} root@${host} ${OpenAMPDir}/ZynQopenAMP.sh 
	
}