CA53=A53
CA9=A9

ifeq ($(target), $(CA53))
 CXX=aarch64-linux-gnu-g++-8
 CFLAGS =
 LDLIBS =
 $(info Default flags provided `for $(shell $(CXX) -dumpmachine).$(target) )
endif

OBJD_DIR=./build.${target}
CFLAGS += --std=c++17 -Wno-write-strings -O3
LDLIBS += -lpthread

$(APP): $(APP_OBJS)
	$(CXX) $(LDFLAGS) -o $@ $(addprefix ${OBJD_DIR}/,$(APP_OBJS)) $(LDLIBS)

vpath %.o ${OBJD_DIR}
%.o: %.cpp
	$(CXX) -c $(CFLAGS) -o ${OBJD_DIR}/$@ $<

install: $(APP)
	@install.sh $(APP)
