#!/bin/bash
C4='\033[00;34m'
C5='\033[00;32m'
BD='\033[00;1m'
RC='\033[0m'

printf "\n"
date

ElfImage="image_echo_test"
Target="remoteproc0"
export RProc="/sys/class/remoteproc/${Target}/"

if [ "$1" == "recycle" ]; then
  printf "ReCycling <${C4}${Target}${RC}> processor\n"
  echo stop > ${RProc}/state
  echo start > ${RProc}/state
  exit;
fi

printf "Firmware on ${C4}${Target}${RC}(/lib/firmware/) -> ${C5}`cat ${RProc}/firmware`${RC}\n"
printf "re-addressed to ${C5}${ElfImage}${RC}\n"
echo ${ElfImage} > ${RProc}/firmware
echo start > ${RProc}/state



