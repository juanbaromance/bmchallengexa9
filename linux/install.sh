#!/bin/bash
COLOR4='\033[00;34m'
COLOR5='\033[00;32m'
BOLD='\033[00;1m'
RESET_COLOR='\033[0m'

host="192.6.1.131"

SCP="scp"
ssh="ssh"
host="10.0.2.15"

remote_path="/opt/shared/bin/"
remote_path="/opt/shared/bin/openAMP"
# remote_path="/lib/firmware"

target=$1
[ -z "${target}" ]&& target="echo_test.x";

SCP="scp -P 1440"
SSH="ssh -p 1440"
host="localhost"

[ ! -e ${target} ] && target="build.bma9/${target}";

printf "\n"
date

printf "Installing ${BOLD}${COLOR4}${target}${RESET_COLOR} on ${host}:${remote_path}\n"

${SCP} ${target} root@${host}:${remote_path};

if [ $? == "1" ]; then
   printf "\n${BOLD}${COLOR5}Remounting/Checking${RESET_COLOR} endpoint ${host} as ${COLOR5}rw${RESET_COLOR} : retry afterwards please\n\n"
   ssh root@$host "mount -o remount,rw /;[ ! -d ${remote_path} ] && mkdir -p ${remote_path}"
else
   ${SSH} root@${host} ${remote_path}/ZynQopenAMP.sh recycle
fi
