/*
 * module.h
 *
 *  Created on: Mar 11, 2020
 *      Author: juanba
 */

#ifndef SRC_MODULE_H_
#define SRC_MODULE_H_


#ifdef __cplusplus
extern "C" {
#endif

void myclass();

#ifdef __cplusplus
}
#endif


#endif /* SRC_MODULE_H_ */
