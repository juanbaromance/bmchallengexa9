#pragma once

#include <valarray>
#include <tuple>
#include <string>

class iArc {
public:
    iArc( const size_t aperture_ = 360, const size_t phulcro_ = 110 );
    iArc& setAperture ( const size_t deg_ );
    iArc& setFulcrum  ( const size_t phulcro_ );

    using AngularDof = std::valarray<double>;
    using CordDof    = std::valarray<double>;
    using RadialDof  = std::valarray<double>;

    enum {
        Angular,
        Cord,
        Radial
    };

    using profileT = std::tuple<AngularDof,CordDof,RadialDof>;
    profileT profile( const std::string &backtrace = "" );
    iArc &Offset( const size_t cord, const size_t radial, const size_t angular );

private:
    size_t aperture, phulcro, density;
    size_t x_offset, y_offset, angular_offset;
    profileT current_profile;
};


