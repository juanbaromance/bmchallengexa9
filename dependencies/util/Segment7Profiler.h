#pragma once

#include <bitset>
#include "util.h"

/* https://doi.org/10.3390/electronics8060652
 * A New Seven-Segment Profile Algorithm for an Open Source Architecture in a Hybrid Electronic Platform
 *
 * Case of Test
 * Input specifications
 * Desired position θd 12π
 * Time of displacement T 1.8 s
 * Time factor for acceleration γ 0.4

 * Static Outputs
 * Time factor for jerk phase φ 0.25
 * Acceleration time 0.72 s
 * Deceleration time 0.18 s
 * Velocity ω 34.9065 rad/s
 * Acceleration α 64.6418 rad/s 2
 * Jerk J 359.1212 rad/s 3
*/

class S7Profiler : public Module<S7Profiler> {

public :
    enum {
        AccOObounds,VelOOBounds,UnConfig,
        NumOfErrors,
    };

    S7Profiler();
    S7Profiler& setAccLimit  ( double acc  );
    S7Profiler& setVelLimit  ( double vel  );
    S7Profiler& setJerkRatio ( double jerk_factor );

    void report( double target, const std::string &backtrace = "" ) const;
    struct point { double t,p,v,a,j; };
    using Buffer = void (*)( const struct point & );
    bool generator( double position_target, double deadline, Buffer e_buffer = nullptr, double position_orig = 0, double velocity_orig = 0 );

private:
    static constexpr auto default_jerk = 4/10.;
    static constexpr auto phi = 1/4.;
    static constexpr auto step_msec = 0.005;
    static constexpr std::bitset<2> Settled = ( 1 << AccOObounds | 1 << VelOOBounds );

    bool beyondLimits( double target, double orig , double deadline );
    S7Profiler & setLimit( size_t limit );
    struct oobounds {
        double acc, vel;
        std::bitset<2> settled;
    }bounds;
    double jerk_factor;
    double velocity, acc, jerk, t_acc, t_jerk, distance, deadline;

    std::bitset<3> error;

};
