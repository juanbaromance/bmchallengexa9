/*
 * Copyright (c) 2018 Xilinx, Inc. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/**************************************************************************
 * FILE NAME
 *
 *       zynq_a9_rproc.c
 *
 * DESCRIPTION
 *
 *       This file define Xilinx Zynq A9 platform specific remoteproc
 *       implementation.
 *
 **************************************************************************/

#include <openamp/remoteproc.h>
#include <metal/atomic.h>
#include <metal/device.h>
#include <metal/irq.h>
#include "platform_info.h"
#include <xil_printf.h>
#include "rproc.h"

/* SCUGIC macros */
#define GIC_DIST_SOFTINT                   0xF00
#define GIC_SFI_TRIG_CPU_MASK              0x00FF0000
#define GIC_SFI_TRIG_SATT_MASK             0x00008000
#define GIC_SFI_TRIG_INTID_MASK            0x0000000F
#define GIC_CPU_ID_BASE                    (1 << 4)

#ifndef nullptr
#define nullptr NULL
#endif

typedef struct remoteproc remoteproc;
typedef struct remoteproc_priv remoteproc_priv;
static remoteproc *zynq_a9_rproc_init( remoteproc *rproc, struct remoteproc_ops *ops, void *arg );
static void *zynq_a9_rproc_mmap   ( remoteproc *rproc, metal_phys_addr_t *pa, metal_phys_addr_t *da, size_t size, unsigned int attribute, struct metal_io_region **io);
static void  zynq_a9_rproc_remove ( remoteproc *rproc);
static int   zynq_a9_rproc_notify ( remoteproc *rproc, uint32_t id);

/* processor operations from r5 to a53. It defines
 * notification operation and remote processor managementi operations. */
struct remoteproc_ops zynq_a9_proc_ops = {
    .init   = zynq_a9_rproc_init,
    .remove = zynq_a9_rproc_remove,
    .mmap   = zynq_a9_rproc_mmap,
    .notify = zynq_a9_rproc_notify,
    .start = NULL,
    .stop = NULL,
    .shutdown = NULL,
};

static int zynq_a9_proc_irq_handler( int vect_id, void *data )
{
    (void)vect_id;
    if ( data == nullptr )
        return METAL_IRQ_NOT_HANDLED;

    struct remoteproc_priv *prproc = ( ( remoteproc* )data )->priv;
    atomic_flag_clear( & prproc->nokick );
	return METAL_IRQ_HANDLED;
}

static remoteproc *zynq_a9_rproc_init( remoteproc *rproc, struct remoteproc_ops *ops, void *arg)
{
    remoteproc_priv *prproc = arg;
    unsigned int irq_vect;

	if (!rproc || !prproc || !ops)
		return NULL;

    struct metal_device *dev;
    int ret = metal_device_open(prproc->gic_bus_name, prproc->gic_name, & dev);
	if (ret) {
		xil_printf("failed to open GIC device: %d.\r\n", ret);
		return NULL;
	}
	rproc->priv = prproc;
	prproc->gic_dev = dev;
	prproc->gic_io = metal_device_io_region(dev, 0);
	if (!prproc->gic_io)
		goto err1;
	rproc->ops = ops;

    atomic_flag_test_and_set( & prproc->nokick );
	/* Register interrupt handler and enable interrupt */
	irq_vect = prproc->irq_notification;
    metal_irq_register( irq_vect, zynq_a9_proc_irq_handler, rproc );
    metal_irq_enable( irq_vect );
    xil_printf("%s: PU%d %s($%08p) metalling irq.vector %2d : SUCCESS\n",
               __PRETTY_FUNCTION__ , prproc->cpu_id, prproc->gic_name, prproc->gic_io->physmap, irq_vect );
	return rproc;

err1:
	metal_device_close(dev);
	return NULL;
}

static void zynq_a9_rproc_remove( remoteproc *rproc )
{
    if ( rproc )
    {
        if( rproc->priv )
        {
            remoteproc_priv *prproc = rproc->priv;
            metal_irq_disable( prproc->irq_to_notify);
            metal_irq_unregister( prproc->irq_to_notify);
            if ( prproc->gic_dev )
                metal_device_close(prproc->gic_dev);
        }
    }
}

static void *zynq_a9_rproc_mmap( remoteproc *rproc, metal_phys_addr_t *pa, metal_phys_addr_t *da, size_t size, unsigned int attribute, struct metal_io_region **io)
{
    metal_phys_addr_t lpa = *pa, lda = *da;

	if (lpa == METAL_BAD_PHYS && lda == METAL_BAD_PHYS)
        return nullptr;

	if (lpa == METAL_BAD_PHYS)
		lpa = lda;
	if (lda == METAL_BAD_PHYS)
		lda = lpa;

    if ( ! attribute )
		attribute = NORM_NONCACHE | STRONG_ORDERED;

    struct remoteproc_mem *mem = metal_allocate_memory(sizeof(*mem));
	if (!mem)
		return NULL;

    struct metal_io_region *tmpio = metal_allocate_memory(sizeof(*tmpio));
	if (!tmpio) {
		metal_free_memory(mem);
		return NULL;
	}
	remoteproc_init_mem(mem, NULL, lpa, lda, size, tmpio);
	/* va is the same as pa in this platform */
	metal_io_init(tmpio, (void *)lpa, &mem->pa, size,sizeof(metal_phys_addr_t)<<3, attribute, NULL);
	remoteproc_add_mem(rproc, mem);
	*pa = lpa;
	*da = lda;
	if (io)
		*io = tmpio;

    xil_printf("%s: metalling $%08x(%6d Kbytes)(%s)\n", __PRETTY_FUNCTION__ , mem->pa, size >> 10, io ? "io" : "mem" );
    return metal_io_phys_to_virt( tmpio, mem->pa );
}

static int zynq_a9_rproc_notify( remoteproc *rproc, uint32_t id)
{
	(void)id;
	if (!rproc)
		return -1;
    struct remoteproc_priv *prproc = rproc->priv;
	if (!prproc->gic_io)
		return -1;

    unsigned long mask =
        ((1 << (GIC_CPU_ID_BASE + prproc->cpu_id)) |(prproc->irq_to_notify))
        & (GIC_SFI_TRIG_CPU_MASK | GIC_SFI_TRIG_INTID_MASK);
	/* Trigger IPI */
	metal_io_write32(prproc->gic_io, GIC_DIST_SOFTINT, mask);
	return 0;
}


