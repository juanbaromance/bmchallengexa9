/*
 * Copyright (c) 2014, Mentor Graphics Corporation
 * All rights reserved.
 *
 * Copyright (c) 2015 Xilinx, Inc. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "xparameters.h"
#include "xil_exception.h"
#include "xil_printf.h"
#include "xscugic.h"
#include "xil_cache.h"
#include <metal/sys.h>
#include <metal/irq.h>
#include "platform_info.h"
static int app_gic_initialize(void);

int init_system(void)
{
    /* Low level abstraction layer for openamp initialization */
    struct metal_init_params metal_param = METAL_INIT_DEFAULTS;
    metal_init( & metal_param );

	/* configure the global interrupt controller */
	app_gic_initialize();

	/* Initialize metal Xilinx IRQ controller */
    int ret = metal_xlnx_irq_init();
    xil_printf("%s: Xilinx metal IRQ controller initialise : %s\n", __PRETTY_FUNCTION__ , ret == 0  ? "SUCCESS" : "FAILURED" );
	return ret;
}

/* Interrupt Controller setup */
static int app_gic_initialize(void)
{
#define INTC_DEVICE_ID		XPAR_SCUGIC_0_DEVICE_ID

    Xil_ExceptionDisable();

    /* Initialize the interrupt controller driver */
    XScuGic_Config *IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
    if (NULL == IntcConfig) {
        return XST_FAILURE;
    }

    static XScuGic xInterruptController;
    u32 Status = XScuGic_CfgInitialize(&xInterruptController, IntcConfig, IntcConfig->CpuBaseAddress);
    if (Status != XST_SUCCESS) {
        return XST_FAILURE;
    }

    /*
     * Register the interrupt handler to the hardware interrupt handling
     * logic in the ARM processor.
     */
    Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_IRQ_INT,
                                 (Xil_ExceptionHandler)XScuGic_InterruptHandler,
                                 &xInterruptController);

    /* Disable the interrupt before enabling exception to avoid interrupts
     * received before exception is enabled.
     */
    XScuGic_Disable(&xInterruptController, SGI_NOTIFICATION);

    Xil_ExceptionEnable();

    /* Connect notificaiton interrupt ID with ISR */
    XScuGic_Connect(&xInterruptController, SGI_NOTIFICATION,(Xil_ExceptionHandler)metal_xlnx_irq_isr,(void *)SGI_NOTIFICATION);

    return 0;
}

void cleanup_system()
{
	metal_finish();
	Xil_DCacheDisable();
	Xil_ICacheDisable();
	Xil_DCacheInvalidate();
	Xil_ICacheInvalidate();
}
