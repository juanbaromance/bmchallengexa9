#ifndef PLATFORM_INFO_H_
#define PLATFORM_INFO_H_


#if defined __cplusplus
extern "C" {
#endif

#include <openamp/rpmsg.h>
#include <openamp/remoteproc.h>
#include <openamp/virtio.h>
#include "platform.public.h"

/* SGIs */
#define SGI_TO_NOTIFY		15 /* SGI to notify the remote */
#define SGI_NOTIFICATION	14 /* SGI from the remote */

/* Memory attributes */
#define NORM_NONCACHE 0x11DE2	/* Normal Non-cacheable */
#define STRONG_ORDERED 0xC02	/* Strongly ordered */
#define DEVICE_MEMORY 0xC06	/* Device memory */
#define RESERVED 0x0		/* reserved memory */

/* Shared memory */
#define SHARED_MEM_PA  0x3e800000UL
#define SHARED_MEM_SIZE 0x80000UL
#define SHARED_BUF_OFFSET 0x80000UL

/* Zynq CPU ID mask */
#define ZYNQ_CPU_ID_MASK 0x1UL









#if defined __cplusplus
}
#endif

#endif /* PLATFORM_INFO_H_ */
