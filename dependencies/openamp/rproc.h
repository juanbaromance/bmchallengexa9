#pragma once

#if defined __cplusplus
extern "C" {
#endif

#include <metal/device.h>
/* Remoteproc private data struct */
struct remoteproc_priv {
    const char *gic_name; /* SCUGIC device name */
    const char *gic_bus_name; /* SCUGIC bus name */
    struct metal_device *gic_dev; /* pointer to SCUGIC device */
    struct metal_io_region *gic_io; /* pointer to SCUGIC i/o region */
    unsigned int irq_to_notify; /* SCUGIC IRQ vector to notify the
                     * other end.
                     */
    unsigned int irq_notification; /* SCUGIC IRQ vector received from
                    * other end.
                    */
    unsigned int cpu_id; /* CPU ID */
    atomic_int nokick; /* 0 for kick from other side */
};

#if defined __cplusplus
}
#endif
