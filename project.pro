CONFIG += c++17
INCLUDEPATH  = ./dependencies/include ./dependencies/include/util
INCLUDEPATH += ./dependencies/include/BMCortexA9 ./dependencies/include/openAMPa9
INCLUDEPATH += ./dependencies/include/kernel/include/ ./dependencies/include/kernel/drivers/remoteproc

HEADERS += \
	module.h \
	openamp_testing.h

SOURCES += \
	freertos_hello_world.c \
	linux/kernel/remoteproc_core.c \
	linux/kernel/remoteproc_elf_loader.c \
	linux/kernel/remoteproc_virtio.c \
	linux/kernel/virtio_rpmsg_bus.c \
	linux/kernel/zynq_remoteproc.c \
	linux/kernel/zynqmp_r5_remoteproc.c \
	linux/meta-openamp/recipes-openamp/rpmsg-examples/rpmsg-mat-mul/mat_mul_demo.c \
	linux/meta-openamp/recipes-openamp/rpmsg-examples/rpmsg-proxy-app/proxy_app.c \
	module.cpp \
	openamp_testing.cpp

DISTFILES += \
	CMakeLists.txt \
	linux/system.dts \
	lscript.ld
