#include "openamp_testing.h"

#include <memory>
#include <string>
#include <iostream>
#include <sstream>
#include <bitset>
#include "platform.public.h"
#include "xil_printf.h"
#include "FreeRTOS.h"
#include "task.h"


static constexpr const char* channel_name = "rpmsg-openamp-demo-channel";
static constexpr uint32_t Shutdown = 0xEF56A55A;


class cOpenAMPClass {
    using cClass = cOpenAMPClass;

public:
    cOpenAMPClass()
    {
        csr = 0;
        xTaskCreate( trampoline, __PRETTY_FUNCTION__ , 1024, this, 2, & comm_task );
    }

private:
    TaskHandle_t comm_task{nullptr};
    static std::bitset<4> csr;
    static void trampoline( void *args ){ reinterpret_cast<cClass*>(args)->deployment(); }

    void deployment()
    {
        void *platform;
        if( int err; ( err = platform_init( 0, nullptr, & platform ) == 0 ) )
        {
            rpmsg_device *rpdev = platform_create_vdev( platform, 0, VirtiIOSlave, nullptr, nullptr );
            if( rpdev )
            {
                rpmsg_endpoint *endpoint = platform_create_ept( rpdev, channel_name, 0, 0xFFFFFFFF, epoint_trampoline, epoint_reset );
                csr.set(Online);
                while( csr.test(Online) )
                    platform_poll( platform );
                platform_release_vdev(rpdev, endpoint );
            }
            platform_cleanup( platform );
        }
        else
        {
            std::stringstream oss;
            oss << __PRETTY_FUNCTION__ << "err(" << err << ")";
            dump("");
        }
    }
    enum {
        Online = 0
    };

    static void dump( const std::string & backtrace ){ xil_printf( "%s\n", backtrace.c_str() ); }

    static int epoint_trampoline( struct rpmsg_endpoint *ept, void *data, size_t len, uint32_t src, void *priv )
    { return reinterpret_cast<cOpenAMPClass*>(priv)->epoint_callback(ept, data, len, src ); }
    int epoint_callback( struct rpmsg_endpoint *ept, void *data, size_t len, uint32_t src )
    {
        static constexpr const char* backtrace = __PRETTY_FUNCTION__ ;
        if ( uint32_t tmp = ( *(unsigned int *)data ); tmp == Shutdown )
        {
            dump( std::string( backtrace ) + ": shutdown message received" );
            csr.reset(Online);
            return 0;
        }
        else
        {
            std::stringstream oss;
            oss << std::string( backtrace ) << " : "
                << std::hex << std::bitset<32>(tmp).to_ulong()
                << std::dec << " len(" << len << ")" << "src(" << src << ")";
            dump( oss.str() );
        }
        return platform_xmitt_ept( ept, data, len );
    }

    static void epoint_reset( struct rpmsg_endpoint* )
    {
        static constexpr const char* backtrace = __PRETTY_FUNCTION__ ;
        dump( backtrace );
        csr.reset(Online);
    }

};

std::bitset<4> cOpenAMPClass::csr;
static std::unique_ptr<cOpenAMPClass> rp_decoder;
extern "C" { void openAMPTest(){ rp_decoder = std::make_unique<cOpenAMPClass>(); } }
